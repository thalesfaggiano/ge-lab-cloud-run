package main

import (
	"ge-lab-cloud-run/internal/infra/client"
	"ge-lab-cloud-run/internal/infra/entrypoint"
	"ge-lab-cloud-run/internal/infra/repository"
	"ge-lab-cloud-run/internal/application/config"
	"ge-lab-cloud-run/internal/application/service"
	"ge-lab-cloud-run/internal/infra/entrypoint/controller"
)

func main() {
	conf := config.NewConfig()
	httpClient := client.NewHTTPClient()

	zipCodeRepo := repository.NewZipCodeRepository(httpClient, conf.ViaCepURL)
	weatherRepo := repository.NewWeatherRepository(httpClient, conf.WeatherAPIURL, conf.WeatherAPIKey)

	zipCodeService := service.NewZipCodeService(zipCodeRepo)
	weatherService := service.NewWeatherService(weatherRepo)

	weatherController := controller.NewWeatherController(weatherService, zipCodeService)

	r := entrypoint.SetupRouter(weatherController)
	r.Run(":8080")
}
