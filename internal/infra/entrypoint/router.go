package entrypoint

import (
	"github.com/gin-gonic/gin"
	"ge-lab-cloud-run/internal/infra/entrypoint/controller"
)

func SetupRouter(weatherController *controller.WeatherController) *gin.Engine {
	r := gin.Default()
	r.GET("/clima/:zipcode", weatherController.GetWeather)
	return r
}
