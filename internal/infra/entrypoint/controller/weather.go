package controller

import (
	"net/http"
	"github.com/gin-gonic/gin"
	"ge-lab-cloud-run/internal/application/service"
)

type WeatherController struct {
	weatherService service.WeatherService
	zipCodeService service.ZipCodeService
}

func NewWeatherController(weatherService service.WeatherService, zipCodeService service.ZipCodeService) *WeatherController {
	return &WeatherController{
		weatherService: weatherService,
		zipCodeService: zipCodeService,
	}
}

func (h *WeatherController) GetWeather(c *gin.Context) {
	zipCode := c.Param("zipcode")
	nl := "\n"
	iz := "invalid zipcode\n"
	cz := "can not find zipcode\n"
	if len(zipCode) != 8 {
		//c.JSON(http.StatusUnprocessableEntity, gin.H{"message": "invalid zipcode"})
		//c.String(http.StatusUnprocessableEntity, nl)
		c.String(http.StatusUnprocessableEntity, iz)
		return
	}

	location, err := h.zipCodeService.GetLocationByZipCode(zipCode)
	if err != nil {
		if err.Error() == "can not find zipcode" {
			//c.JSON(http.StatusNotFound, gin.H{"message": "can not find zipcode"})
			//c.String(http.StatusNotFound, nl)
			c.String(http.StatusNotFound, cz)
		} else {
			//c.JSON(http.StatusUnprocessableEntity, gin.H{"message": "invalid zipcode"})
			//c.String(http.StatusUnprocessableEntity, nl)
			c.String(http.StatusUnprocessableEntity, iz)
		}
		return
	}

	weather, err := h.weatherService.GetWeatherByLocation(location.Localidade)
	if err != nil {
		//c.JSON(http.StatusInternalServerError, gin.H{"message": "can not find zipcode"})
		//c.String(http.StatusInternalServerError, nl)
		c.String(http.StatusInternalServerError, cz)
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"temp_C": weather.TempCelsius,
		"temp_F": weather.TempCelsius*1.8 + 32,
		"temp_K": weather.TempCelsius + 273.15,
	})
	c.String(http.StatusOK, nl)
}
