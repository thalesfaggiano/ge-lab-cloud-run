package config

type Config struct {
	ViaCepURL     string
	WeatherAPIURL string
	WeatherAPIKey string
}

func NewConfig() *Config {
	return &Config{
		ViaCepURL:     "https://viacep.com.br/ws",
		WeatherAPIURL: "http://api.weatherapi.com/v1/current.json",
		WeatherAPIKey: "1bfa2f2d1e374ce98b571734243005",
	}
}
