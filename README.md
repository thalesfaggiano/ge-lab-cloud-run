# GE-Lab Cloud Run

- [GE-Lab-Cloud-Run](#ge-lab-cloud-run)
  * [Motivação](#motivação)
  * [Laboratório](#laboratório)
  * [Documentação](#documentação)

## Motivação

Este é um laboratório da Pós-Graduação da Fullcycle: Go Expert, Desenvolvimento Avançado em Golang

## Laboratório

### Objetivo: 
Desenvolver um sistema em Go que receba um CEP, identifica a cidade e retorna o clima atual (temperatura em graus celsius, fahrenheit e kelvin). Esse sistema deverá ser publicado no Google Cloud Run.

### Requisitos:
- O sistema deve receber um CEP válido de 8 digitos
- O sistema deve realizar a pesquisa do CEP e encontrar o nome da localização, a partir disso, deverá retornar as temperaturas e formata-lás em: Celsius, Fahrenheit, Kelvin.
- O sistema deve responder adequadamente nos seguintes cenários:
  - Em caso de sucesso:
    - Código HTTP: 200
    - Response Body: { "temp_C": 28.5, "temp_F": 28.5, "temp_K": 28.5 }
  - Em caso de falha, caso o CEP não seja válido (com formato correto):
    - Código HTTP: 422
    - Mensagem: invalid zipcode
  - Em caso de falha, caso o CEP não seja encontrado:
    - Código HTTP: 404
    - Mensagem: can not find zipcode
  - Deverá ser realizado o deploy no Google Cloud Run.

### Dicas:
- Utilize a API viaCEP (ou similar) para encontrar a localização que deseja consultar a temperatura: https://viacep.com.br/
- Utilize a API WeatherAPI (ou similar) para consultar as temperaturas desejadas: https://www.weatherapi.com/
- Para realizar a conversão de Celsius para Fahrenheit, utilize a seguinte fórmula: F = C * 1,8 + 32
- Para realizar a conversão de Celsius para Kelvin, utilize a seguinte fórmula: K = C + 273
  - Sendo F = Fahrenheit
  - Sendo C = Celsius
  - Sendo K = Kelvin

### Entrega:

- O código-fonte completo da implementação.
- Testes automatizados demonstrando o funcionamento.
- Utilize docker/docker-compose para que possamos realizar os testes de sua aplicação.
- Deploy realizado no Google Cloud Run (free tier) e endereço ativo para ser acessado.

## Documentação

### Quickstart Compose

Em um novo terminal:

Passo 1:

```Bash
git clone git@gitlab.com:thalesfaggiano/ge-lab-cloud-run.git
```

Passo 2:
```Bash
cd ge-lab-cloud-run
```

Passo 3:
```Bash
docker compose up --build
```

Em outro terminal execute os próximos passos para validar a aplicação:

Passo 4:
```Bash
curl -X GET http://localhost:8080/clima/05305030
```

Passo 5:
```Bash
curl -X GET http://localhost:8080/clima/11111111
```

Exemplo de resultados de um teste com CEP Válido, Inválido e Inexistente:
![ge-lab-cloud-run](/images/cloud-run01.jpg)

---
### Quickstart Cloud Run

Passo 1:

```Bash
curl -X GET https://ge-lab-cloud-run-xcxuw6n3zq-uc.a.run.app/clima/05305030
```

Passo 2:
```Bash
curl -X GET  https://ge-lab-cloud-run-xcxuw6n3zq-uc.a.run.app/clima/01153000
```

Passo 3:

Acesse em um browser os endereços:

- https://ge-lab-cloud-run-xcxuw6n3zq-uc.a.run.app/clima/05305030
- https://ge-lab-cloud-run-xcxuw6n3zq-uc.a.run.app/clima/01153000

---
### Testes Google Cloud Run

Seguem exemplos de resultado em um serviço implantado com Cloud Run.

CEP Válido:

![ge-lab-cloud-run](/images/cloud-run02.jpg)


CEP Inválido:

![ge-lab-cloud-run](/images/cloud-run03.jpg)


CEP Inexistente:

![ge-lab-cloud-run](/images/cloud-run04.jpg)
